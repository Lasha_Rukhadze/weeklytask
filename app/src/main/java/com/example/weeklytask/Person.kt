package com.example.weeklytask

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Person(var firstName : String,
                  var lastName : String,
                  var age : Int,
                  var email : String) : Parcelable{

    var personFirstName = firstName
    var personLastName = lastName
    var personAge = age
    var personEmail = email
}
