package com.example.weeklytask

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.example.weeklytask.databinding.ActivityMainBinding
import java.lang.Integer.parseInt
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.schedule

class MainActivity : AppCompatActivity() {

    //var numberOfUsers : Int = 0
    var removedUsers : Int = 0
    var person = Person("", "", 0, "")
    var users = mutableSetOf(person)

    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        click()
    }

    private fun click (){
        binding.adduser.setOnClickListener {
            addUser()
        }

        binding.removeuser.setOnClickListener {
            removeUser()
        }

        binding.updateuser.setOnClickListener {
            update()
        }

        binding.viewInfo.setOnClickListener {
            viewProfile()
        }
    }

    private fun isLetters(string: String): Boolean {
        return string.filter { it in 'A'..'Z' || it in 'a'..'z' }.length == string.length
    }

    private fun check() : Boolean {
        if(binding.firstname.text.toString().isBlank() || binding.lastname.text.toString().isBlank()
            || binding.age.text.toString().isBlank() || binding.email.text.toString().isBlank() || !Patterns.EMAIL_ADDRESS.matcher(binding.email.text.toString()).matches()
            || !isLetters(binding.firstname.text.toString()) || !isLetters(binding.lastname.text.toString())){
            binding.error.visibility = View.VISIBLE
            binding.success.visibility = View.INVISIBLE
            Toast.makeText(this, "To view, add, remove or update user you  must fill all fields with valid information", Toast.LENGTH_SHORT).show()
            return true
        }
        person = Person(binding.firstname.text.toString(), binding.lastname.text.toString(), parseInt(binding.age.text.toString()), binding.email.text.toString())
        return false
    }

    private fun addUser(){
        if(check()) return
        else if (!users.any { it.email == person.email } && users.add(person)) {
            // numberOfUsers++
            binding.numberofusers.text = (users.count()-1).toString()   //numberOfUsers.toString()
            binding.success.visibility = View.VISIBLE
            binding.error.visibility = View.INVISIBLE
            Toast.makeText(this, "User added successfully", Toast.LENGTH_SHORT).show()
        }
        else {
            Toast.makeText(this, "User already exists", Toast.LENGTH_SHORT).show()
            binding.error.visibility = View.VISIBLE
            binding.success.visibility = View.INVISIBLE
        }
    }

    private fun removeUser(){
        if (check()) return
        if(users.indexOf(person)>=0) {
                users.remove(person)
                binding.success.visibility = View.VISIBLE
                binding.error.visibility = View.INVISIBLE
                removedUsers++
               // numberOfUsers--
                binding.numberofremoveds.text = removedUsers.toString()
                binding.numberofusers.text = (users.count()-1).toString()
            Toast.makeText(this, "User removed successfully", Toast.LENGTH_SHORT).show()//numberOfUsers.toString()
        }
        else {
                Toast.makeText(this, "User does not exist", Toast.LENGTH_SHORT).show()
                binding.error.visibility = View.VISIBLE
                binding.success.visibility = View.INVISIBLE
        }
    }

    private fun update() {
        if (check()) return
        if (users.any { it.email == person.email }) {
            users.forEach {
                if (it.email == person.email){
                    users.remove(it)
                    users.add(person)
                    Toast.makeText(this, "User updated successfully", Toast.LENGTH_SHORT).show()
                    binding.success.visibility = View.VISIBLE
                    binding.error.visibility = View.INVISIBLE
                    return
                }
            }
        }
        else {
            Toast.makeText(this, "Such user does not exist to update", Toast.LENGTH_SHORT).show()
            binding.error.visibility = View.VISIBLE
            binding.success.visibility = View.INVISIBLE
        }
    }
    private fun viewProfile() {
        if(check()) return
        if(users.any { it == person }) {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra("Person", person)
           // intent.putParcelableArrayListExtra("Users", ArrayList(users))
            startActivity(intent)
        }
        else {
            Toast.makeText(this, "Such user does not exist", Toast.LENGTH_SHORT).show()
        }
    }
}