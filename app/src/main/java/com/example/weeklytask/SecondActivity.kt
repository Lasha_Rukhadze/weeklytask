package com.example.weeklytask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.weeklytask.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() {

    lateinit var binding: ActivitySecondBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //var myUsers : MutableSet<Person> = intent.getParcelableArrayListExtra<Person>("Users")!!.toMutableSet()
        val authenticatedPerson : Person? = intent.getParcelableExtra<Person>("Person")

        binding.firstNameValue.text = authenticatedPerson?.firstName
        binding.lastNameValue.text = authenticatedPerson?.lastName
        binding.ageValue.text = authenticatedPerson?.personAge.toString()
        binding.emailValue.text = authenticatedPerson?.personEmail
    }
}